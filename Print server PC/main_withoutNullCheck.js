const { app, BrowserWindow, net } = require('electron');
const fs = require('fs');
const request = require('request');
let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({ width: 1200, height: 800 });
  setInterval(printImages, 30000);
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
}

function printImages() {
  downloadJSONs().then(data => {
    if (data.length > 0) {
      data.forEach(element => {
        console.log(element);
        let child = new BrowserWindow({ width: 2400, height: 1800, parent: mainWindow, modal: true, show: false });

        child.loadURL(element.imageUrl);
        setTimeout(function() {});
        child.webContents.on('did-finish-load', () => {
          child.show();
          fs.readFile(__dirname + '/print.css', 'utf-8', function(error, data) {
            if (!error) {
              child.webContents.insertCSS(data);
              setTimeout(function() {
                child.webContents.print({ silent: true }, () => {
                  child.close();
                });
              }, 3000);
              clearJSON(element.code);
            }
          });
        });
      });
    } else {
      console.log('nothing to print');
    }
  });
}

function downloadJSONs() {
  const request = net.request({
    method: 'GET',
    protocol: 'https:',
    hostname: 'beboticssmurfen.lwprod2.nl/retrievePrintQueue.php'
  });
  return new Promise(function(resolve, reject) {
    request.on('response', response => {
      response.on('data', chunk => {
        resolve(eval(chunk.toString()));
      });
    });
    request.end();
  });
}

function clearJSON(cd) {
  // let data = `code=${cd}`;
  const options = {
    method: 'POST',
    url: 'http://beboticssmurfen.lwprod2.nl/clearPrintQueue.php',
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    form: { code: cd }
  };
  request(options, function(error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', function() {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function() {
  if (mainWindow === null) {
    createWindow();
  }
});
