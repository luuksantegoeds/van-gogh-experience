<?php

require_once "lib/helper.php";

//Check for code
if (!isset($_POST["code"]) || !isset($_POST["print"])) {
    print buildOutput(false, null, "missing POST parameter");
    exit;
}

//Load acceptQueue
$jsonPathAccept = getcwd() . "/acceptQueue.json";
$acceptOutput = null;

//Check if file exists & is parcable
if (file_exists($jsonPathAccept)) {
    $jsonAcceptData = file_get_contents($jsonPathAccept);

    try {
        $acceptOutput = json_decode($jsonAcceptData, true);
        if (!$acceptOutput) {
            $acceptOutput = array();
        }

    } catch (Exception $e) {}
} else {
    $acceptOutput = array();
}

//Set data
$code = $_POST['code'];

for ($i = 0; $i < count($acceptOutput); $i++) {
    if ($acceptOutput[$i]['code'] == $code) {
        $key = $i;
        $imageUrl = $acceptOutput[$key]['imageUrl'];
        break;
    }
}

if ($_POST['print'] == 1) {
    $jsonPathPrint = getcwd() . "/printQueue.json";
    $printOutput = null;

    //Check if file exists & is parcable
    if (file_exists($jsonPathPrint)) {
        $jsonPrintData = file_get_contents($jsonPathPrint);

        try {
            $printOutput = json_decode($jsonPrintData, true);
            if (!$printOutput) {
                $printOutput = array();
            }

        } catch (Exception $e) {}
    } else {
        $printOutput = array();
    }

    $printOutput[] = array(
        "code" => $code,
        "imageUrl" => $imageUrl,
    );

    include("increasePrinted.php");
    file_put_contents($jsonPathPrint, json_encode($printOutput));
}

unset($acceptOutput[$key]);

if (file_put_contents($jsonPathAccept, json_encode(array_values($acceptOutput)))) {
    print buildOutput(true, null, "queue data updated");
} else {
    print buildOutput(false, null, "failed to update queue data");
}
