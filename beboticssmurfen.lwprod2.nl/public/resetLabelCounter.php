<?php

  $jsonPath = getcwd()."/labelCounter.json";
  $output = null;
  
  //Check if file exists & is parcable
  if(file_exists($jsonPath)) {
    $jsonData = file_get_contents($jsonPath);
    
    try {
      $output = json_decode($jsonData, true);
      if(!$output){
          $output = array();
      }
      else{
        $output['counter'] = 0;
        file_put_contents($jsonPath, json_encode($output));
      }
    }
    catch(Exception $e) { }
  }
  else
    $output = array();
  
  //Return verified json
  print json_encode($output);

?>