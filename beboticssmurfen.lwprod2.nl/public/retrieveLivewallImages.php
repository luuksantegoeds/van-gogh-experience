<?php
  require_once("lib/helper.php");

  header("Access-Control-Allow-Origin: *");

  // Find image files and put them into array
  $files = array();
  $remoteStream = opendir(getcwd()."/modified/");
  while(($fileName = readdir($remoteStream)) !== false) {
    if ($fileName != "." && $fileName != "..") {
      $filePath = getcwd()."/modified/".$fileName;
      $filePathExploded = explode(".", $fileName);
      $fileExtension = end($filePathExploded);

      $fileObject = new stdClass();
      $fileObject->code = basename($fileName, ".".$fileExtension);
      $fileObject->imagePath = "https://beboticssmurfen.lwprod2.nl/modified/$fileName";

      $files[filemtime($filePath)] = $fileObject;
    }
  }
  closedir($remoteStream);

  // Sort images by key(date of modification)
  ksort($files);

  // Get last 10 files from $files array
  $returnFiles = array_slice($files, -50);
  if (!$returnFiles) $returnFiles = array();

  //Return verified json
  print json_encode(array_values($returnFiles));
?>
