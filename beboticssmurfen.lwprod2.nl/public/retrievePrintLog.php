<?php

  $jsonPath = getcwd()."/printLog.json";
  $output = null;
  
  //Check if file exists & is parcable
  if(file_exists($jsonPath)) {
    $jsonData = file_get_contents($jsonPath);
    
    try {
      $output = json_decode($jsonData, true);
      if(!$output)
        $output = array();
    }
    catch(Exception $e) { }
  }
  else
    $output = array();
  
  print("Days:<br>");
  print("----<br>");
  foreach($output['days'] as $day){
    print("<br>");
    print("Date: " . $day['date'] . "<br>");
    print("Amount requested: " . $day['amountRequested'] . "<br>");
    print("Amount printed: " . $day['amountPrinted'] . "<br>");
  }

?>