<?php

$jsonPath = getcwd() . "/printLog.json";
$output = null;

//Check if file exists & is parcable
if (file_exists($jsonPath)) {
    $jsonData = file_get_contents($jsonPath);

    try {
        $output = json_decode($jsonData, true);
        if (!$output) {
            $output = array();
        } else {
            $currentDate = date("Y-m-d");

            $logCurrentDateExists = false;
            foreach ($output['days'] as $day) {
                if ($day['date'] == $currentDate) {
                    $key = array_search($day, $output['days']);
                    $output['days'][$key]['amountRequested']++;
                    $logCurrentDateExists = true;
                }
            }

            if (!$logCurrentDateExists) {
                $newDay = [];
                $newDay['date'] = $currentDate;
                $newDay['amountRequested'] = 1;
                $newDay['amountPrinted'] = 0;
                array_push($output['days'], $newDay);
            }

            file_put_contents($jsonPath, json_encode($output));
        }

    } catch (Exception $e) {}
} else {
    $output = array();
}

//Return verified json
print json_encode($output);
