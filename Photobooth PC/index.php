<html>
<head>
    <title>van Gogh Experience Back-end Service</title>
    <meta http-equiv="Refresh" content="3"> <!-- Reload page after 3 seconds -->
	<script src = "DYMO.Label.Framework.latest.js" type="text/javascript" charset="UTF-8"> </script>
	<script type="text/javascript" src="jquery-1.8.3.js"></script>
</head>
<body>
<center>
    <h1>Sluit dit scherm NIET!</h1>
</center>
</body>
</html>

<?php

function getLatestId()
{
    $idFile = fopen("latestId.txt", "r") or die("latestId.txt not found!");
    return fread($idFile, filesize("latestId.txt"));
}

function idToString($id)
{
    if ($id < 10) {
        $id = "000" . $id;
    } else if ($id < 100) {
        $id = "00" . $id;
    } else if ($id < 1000) {
        $id = "0" . $id;
    }

    return $id;
}

function increaseLatestId()
{
    $latestId = getLatestId() + 1;
    $latestId = idToString($latestId);
    $idFile = fopen("latestId.txt", "w") or die("latestId.txt not found!");
    fwrite($idFile, $latestId);
    fclose($idFile);
}

function sendFileToServer($path, $file)
{
    try {
        $server = "192.168.1.3";
        $username = "pi";
        $password = "bebotics";

        set_include_path(getcwd());
        set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
        include 'Net/SFTP.php';
        define('NET_SFTP_LOGGING', NET_SFTP_LOG_COMPLEX); // or NET_SFTP_LOG_SIMPLE

        $sftp = new Net_SFTP($server);
        $sftp->login($username, $password);
        $sftp->put(("/home/pi/files-vangogh/" . $file), ($path . $file), NET_SFTP_LOCAL_FILE);
        unset($sftp);
    } catch (Exception $e) {}
}

//Makes the server fetch the slider image
function triggerServer($code){
    file_get_contents("http://192.168.1.3/retrieveEditorImage.php/?code=$code");
}

function run()
{
    $path = "C:\Users\Van Gogh\Documents\Photobooth prints\\";
    $files = scandir($path, SCANDIR_SORT_DESCENDING);
    unset($files[array_search("..", $files)]);
    unset($files[array_search(".", $files)]);

    if (count($files) == 0) {
        die("No files in directory");
    }

    $newFile = $files[0];
    if (strlen($newFile) <= 8) {
        die("No new files in directory");
    }

    if(filesize("C:\Users\Van Gogh\Documents\Photobooth prints\\" . $newFile) < 1048576){ //Check to make sure only photos get uploaded to the server, format in bytes (1 MB)
        die("No new files in directory");
    }

    $newId = getLatestId() + 1;
    increaseLatestId();
    $realCode = $newId;

    $code = idToString($newId);
    $idToTrigger = $code;
    $code = substr($code, 0, 1) . " " . substr($code, 1, 1) . " " . substr($code, 2, 1) . " " . substr($code, 3, 1);
    echo "<textarea id='code' hidden>$code</textarea>";

    $newId = idToString($newId) . ".jpg";
    rename(($path . $newFile), ($path . $newId));

    sendFileToServer($path, $newId);
    triggerServer($idToTrigger);
}

function printLabel()
{
    echo '<script src = "dymo.js" type="text/javascript" charset="UTF-8"> </script>';
}

run();

$handle = fopen("template.xml", "r") or die("Couldn't get handle");
$content = "";
if ($handle) {
    while (!feof($handle)) {
        $buffer = fgets($handle, 4096);
        $content .= $buffer;
    }
    fclose($handle);
}

echo "<textarea id='xml' hidden>$content</textarea>";
printLabel();

// Increase label counter
file_get_contents("http://192.168.1.3/increaseLabelCounter.php");

?>