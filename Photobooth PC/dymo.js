//----------------------------------------------------------------------------
//  DYMO Label Framework JavaScript Library Samples: Print label
//  Copyright (c), 2010, Sanford, L.P. All Rights Reserved.
//----------------------------------------------------------------------------
 
(function()
{
	// called when the document completly loaded
    function onload()
    {
		var xml = "";
		$(document).ready(function () {
		var val = $.trim($("#xml").val());
		if (val != "") {
			xml = val;
		}
		});
        var printKnop = document.getElementById('printKnop');
 
        // prints the label
       try
            {
				// get code
				var code = "";
				$(document).ready(function () {
				var val = $.trim($("#code").val());
				if (val != "") {
					code = val;
				}
				});
				
                // change code
                var labelXml = xml.replace("C O D E", code);
				
                // open label
                var label = dymo.label.framework.openLabelXml(labelXml);
 
                // select printer to print on
                // for simplicity sake just use the first LabelWriter printer
                var printers = dymo.label.framework.getPrinters();
                if (printers.length == 0)
                    throw "No DYMO printers are installed. Install DYMO printers.";
 
                var printerName = "";
                for (var i = 0; i < printers.length; ++i)
                {
                    var printer = printers[i];
                    if (printer.printerType == "LabelWriterPrinter")
                    {
                        printerName = printer.name;
                        break;
                    }
                }
 
                if (printerName == "")
                    throw "No LabelWriter printers found. Install LabelWriter printer";
 
                // finally print the label
                label.print(printerName);
            }
            catch(e)
            {
                alert(e.message || e);
            }
    };
 
    // register onload event
    if (window.addEventListener)
        window.addEventListener("load", onload, false);
    else if (window.attachEvent)
        window.attachEvent("onload", onload);
    else
        window.onload = onload;
 
} ());