<?php

$jsonPath = getcwd() . "/printQueue.json";
$output = null;

//Check if file exists & is parcable
if (file_exists($jsonPath)) {
    $jsonData = file_get_contents($jsonPath);

    try {
        $output = json_decode($jsonData, true);
        if (!$output) {
            $output = array();
        } else{
          $keysToUnset = [];
          $counter = 0;
          foreach($output as $item){
            if($item['imageUrl'] == null){
              array_push($keysToUnset, $counter);
            } else if($item['imageUrl'] == "null"){
              array_push($keysToUnset, $counter);
            }
            $counter++;
          }

          foreach($keysToUnset as $key){
            unset($output[$key]);
          }

          if(count($keysToUnset) != 0){
            file_put_contents($jsonPath, json_encode(array_values($output)));
          }
        }

    } catch (Exception $e) {}
} else {
    $output = array();
}

//Return verified json
print json_encode(array_values($output));