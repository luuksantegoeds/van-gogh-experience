<?php
  
  require_once("lib/helper.php");
  
  //Check for data
  if(!isset($_POST["code"]) || !isset($_POST["imageData"])) {
    print buildOutput(false, null, "missing POST parameters");
    exit;
  }
  
  //Split data for image in half (if it doesn't return 2 parts, it is most likely invalid)
  $imageParts = explode(";base64,", $_POST["imageData"]);
  if(count($imageParts) !== 2) {
    print buildOutput(false, null, "invalid base64 data received");
    exit;
  }
  
  //Check if there is valid mimetype (any image)
  $mimeTypeParts = explode("/", $imageParts[0]);
  if(count($mimeTypeParts) !== 2 || !strpos($mimeTypeParts[0], "image")) {
    print buildOutput(false, null, "unsupported base64 content type");
    exit;
  }
  
  //Get extension
  $extension = $mimeTypeParts[1];
  if($extension === "jpeg")
    $extension = "jpg";
  
  //Attempt decode
  $imageData = base64_decode($imageParts[1], true);
  if(!$imageData) {
    print buildOutput(false, null, "failed to decode base64 data");
    exit;
  }
  
  //Set path / filename
  $storePath = getcwd()."/modified/";
  $fileName = $_POST["code"].".$extension";
  
  //Save image
  if(file_put_contents($storePath.$fileName, $imageData)) {
    print buildOutput(true, "http://192.168.1.3/modified/$fileName");
  }
  else {
    print buildOutput(false, null, "failed to save image to file");
  }
  
  

?>