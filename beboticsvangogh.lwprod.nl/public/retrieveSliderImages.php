<?php
  require_once("lib/helper.php");

  header("Access-Control-Allow-Origin: *");

  // Find image files and put them into array
  $files = array();
  $remoteStream = opendir(getcwd()."/original/");
  while(($fileName = readdir($remoteStream)) !== false) {
    if ($fileName != "." && $fileName != "..") {
      $filePath = getcwd()."/original/".$fileName;
      $filePathExploded = explode(".", $fileName);
      $fileExtension = end($filePathExploded);

      $fileObject = new stdClass();
      $fileObject->code = basename($fileName, ".".$fileExtension);
      $fileObject->imagePath = "http://192.168.1.3/original/$fileName";

      $files[filemtime($filePath)] = $fileObject;
    }
  }
  closedir($remoteStream);

  // Sort images by key(date of modification)
  ksort($files);

  // Get last 10 files from $files array
  $returnFiles = array_slice($files, -10);
  if (!$returnFiles) $returnFiles = array();

  //Return verified json
  print json_encode(array_values($returnFiles));
?>
