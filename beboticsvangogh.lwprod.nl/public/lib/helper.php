<?php

  //Enable error reporting
  error_reporting(-1);
  ini_set("error_reporting", E_ALL);
  
  //Simple output builder
  function buildOutput($success, $data = null, $error = null) {
    $output = array("success" => $success);
    if($data)
      $output["data"] = $data;
    if($error)
      $output["error"] = $error;
    return json_encode($output);
  }

?>