<?php

  require_once("lib/helper.php");
  
  //Try to load JSON
  $jsonPath = getcwd()."/acceptQueue.json";
  $jsonContents = json_encode(array());
  
  //Store data again
  if(file_put_contents($jsonPath, $jsonContents))
    print buildOutput(true, null, "queue data cleared");
  else
    print buildOutput(false, null, "failed to clear queue data");
  

?>